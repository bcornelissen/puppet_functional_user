define functional_user (
  $id,
  $password  = undef,
  $system    = false,
  $home      = "/home/${name}",
  $shell     = '/bin/sh',
  $home_mode = '0700',
  $groups    = [], # Groups should only be used for non-ldap groups (does not create the group)
  $pubkey    = undef,
) {

  validate_bool($system)

  group { $name:
    ensure  => present,
    system  => $system,
    gid     => $id,
  }

  user { $name:
    ensure      => present,
    uid         => $id,
    gid         => $id,
    password    => $password,
    home        => $home,
    managehome  => true,
    shell       => $shell,
    system      => $system,
    groups      => $groups,
    require     => Group[$name, $groups],
  }

  # User home only gets created with the useradd command, which is not run if the user already exists (when it also exists in LDAP)
  file { $home:
    ensure => directory,
    owner  => $name,
    group  => $name,
    mode   => $home_mode,
  }

  file { "${home}/.ssh":
    ensure  => directory,
    owner   => $name,
    group   => $name,
    mode    => $home_mode,
    require => File[$home],
  }

  if $pubkey {
    file { "${home}/.ssh/authorized_keys":
      ensure  => file,
      owner   => $name,
      group   => $name,
      mode    => 0600,
      content => $pubkey,
      require => File["${home}/.ssh"],
    }
  }
}
